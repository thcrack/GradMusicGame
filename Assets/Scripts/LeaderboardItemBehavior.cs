﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardItemBehavior : MonoBehaviour {

	public Text rankText;
	public Text idText;
	public Text scoreText;
	
	public void UpdateText(int rank, string id, int score){
		rankText.text = "#" + rank.ToString();
		idText.text = id;
		scoreText.text = score.ToString();
	}

	public void ClearText(){
		rankText.text = idText.text = scoreText.text = "";
	}

	public void SetNormalColor(){
		rankText.color = idText.color = scoreText.color = Color.white;
	}

	public void SetSelfColor(){
		rankText.color = idText.color = scoreText.color = Color.cyan;
	}

	public void Set2PColor(){
		rankText.color = idText.color = scoreText.color = Color.magenta;
	}
}
