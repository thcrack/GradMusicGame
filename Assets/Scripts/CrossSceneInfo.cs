using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.SerializableAttribute]
public class ResultScore {
	public int resultDefenseScore;
	public int resultAttackScore;
	public int resultTotalScore;
	public int resultHitCount;
	public int resultTotalCount;
	public float resultHitRatio;

	public ResultScore(PlayerScore playerScore){
		resultAttackScore = Mathf.CeilToInt(playerScore.scoreAttack);
		resultDefenseScore = Mathf.CeilToInt(playerScore.scoreHit);
		resultTotalScore = Mathf.CeilToInt(playerScore.scoreAttack) + Mathf.CeilToInt(playerScore.scoreHit);
		resultHitCount = playerScore.hitCount;
		resultTotalCount = playerScore.totalCount;
		resultHitRatio = (playerScore.totalCount != 0) ? (float)playerScore.hitCount/(float)playerScore.totalCount : 0;
	}
}

public class CrossSceneInfo : Singleton<CrossSceneInfo> {

	public string loggedID;
	public string loggedIDDuoA;
	public string loggedIDDuoB;
	public GameMode selectedGameMode;
	public GameTrack selectedTrack;
	public GameRecordEntry lastRecord;
	public GameRecordEntry lastRecordDuoA;
	public GameRecordEntry lastRecordDuoB;

	public ResultScore singlePlayerResultScore;
	public ResultScore duoPlayerResultScoreA;
	public ResultScore duoPlayerResultScoreB; 

	public Dictionary<ColorPrefType, Color> originalColorPref;

	public void ChangeScene(string sceneName){
		SceneManager.LoadScene(sceneName);
	}

	public void SetColorPref(Dictionary<ColorPrefType, Color> input){
		originalColorPref = new Dictionary<ColorPrefType, Color>(input);
	}
}
