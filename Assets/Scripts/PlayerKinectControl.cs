using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;

/**
TODO

2 player body assignment
set all playermovement controllable to false if in duo mode
set duo outer arm object to disactive if in sp mode


**/

public class PlayerKinectControl : MonoBehaviour {

	BodySourceManager bodyManager;
	PlayerMovement playerMovement;

	[Header("Lean Properties")]
	public float leanSensitivity = 0.8f;
	public float leanMinimumGate = 0.1f;

	[Header("Hand Properties")]
	[Range(0f, 1f)]
	public float centerHeadWristRatio = 0.25f;

	[Header("Crouch Properties")]
	public float crouchAngleThreshold = 135f;
	public bool isCrouching = false;

	[Header("Debug Texts")]
	public Text leanAngleText;

	Vector3 headPos, leftHandPos, rightHandPos, wristPos;
	Vector3 leftHipPos, leftKneePos, leftFootPos;
	Vector3 rightHipPos, rightKneePos, rightFootPos;
	public PlayerIndicatorManager playerIndicatorManager;
	GameManager gameManager;
	public Body trackedBody;
	float leanResult = 0f;

	// Use this for initialization
	void Start () {

		bodyManager = Global.Instance._kinectBodyManager;
		playerMovement = GetComponent<PlayerMovement>();
		gameManager = Global.Instance._gameManager;
		
	}
	
	// Update is called once per frame
	void Update () 
	{

		Body[] bodyData = bodyManager.GetData();
		if(bodyData != null)
		{

			if(trackedBody==null || !trackedBody.IsTracked)
			{
				trackedBody = DetermineTrackingBody(bodyData);

			}else{

				// Set Positions

				MapUpperBody(trackedBody);
				MapLowerBody(trackedBody);

				/** Lean **/
				leanResult = trackedBody.Lean.X * leanSensitivity;
				if(gameManager.gameMode != GameMode.Duo && (leanResult >= leanMinimumGate || leanResult <= -leanMinimumGate)) playerMovement.RotatePlayer(leanResult);

				/** Hands Position **/

				//Use the distance between head and wrist as the width and height of playable area


				float detectAreaSideLength = Vector3.Distance(headPos, wristPos);
				if(detectAreaSideLength > 0)
				{
					Vector3 midPoint = Vector3.Lerp(headPos, wristPos, centerHeadWristRatio);
					Vector3 leftHandCenterDiff = leftHandPos - midPoint;
					Vector3 rightHandCenterDiff = rightHandPos - midPoint;

					var leftHandResult = new Vector2((leftHandCenterDiff.x + detectAreaSideLength/2)/detectAreaSideLength
												,(leftHandCenterDiff.y + detectAreaSideLength/2)/detectAreaSideLength);

					var rightHandResult = new Vector2((rightHandCenterDiff.x + detectAreaSideLength/2)/detectAreaSideLength
												,(rightHandCenterDiff.y + detectAreaSideLength/2)/detectAreaSideLength);

					playerIndicatorManager.UpdateHandsPosition(leftHandResult, rightHandResult);

				}

				/** Crouch Detection **/

				float leftKneeAngle = Vector3.Angle(leftFootPos - leftKneePos, leftHipPos - leftKneePos);
				float rightKneeAngle = Vector3.Angle(rightFootPos - rightKneePos, rightHipPos - rightKneePos);
				leanAngleText.text = leftKneeAngle + " / " + rightKneeAngle;

				isCrouching = (leftKneeAngle <= crouchAngleThreshold && rightKneeAngle <= crouchAngleThreshold);
				if(isCrouching && playerIndicatorManager.chargeBar.FullyCharged){
					playerIndicatorManager.chargeBar.ReleaseCharge();
				}

			}
		}
	}

	void MapUpperBody(Body body){
		headPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.Head].Position);
		leftHandPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.HandLeft].Position);
		rightHandPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.HandRight].Position);
		wristPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.SpineBase].Position);
		leftHandPos.z = rightHandPos.z = wristPos.z = headPos.z;
	}

	void MapLowerBody(Body body){
		leftFootPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.FootLeft].Position);
		leftKneePos = Util.KinectCamSpaceToVector3(body.Joints[JointType.KneeLeft].Position);
		leftHipPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.HipLeft].Position);
		rightFootPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.FootRight].Position);
		rightKneePos = Util.KinectCamSpaceToVector3(body.Joints[JointType.KneeRight].Position);
		rightHipPos = Util.KinectCamSpaceToVector3(body.Joints[JointType.HipRight].Position);
	}

	public float GetLean{
		get
		{
			return leanResult;
		}
	}

	Body DetermineTrackingBody(Body[] bodyData){
		foreach(var body in bodyData){
			if(!body.IsTracked) continue;
			if(gameManager.gameMode == GameMode.Duo && gameManager.CheckBodyAssignment(body, this)) continue;
			MapUpperBody(body);
			if(leftHandPos.y >= headPos.y && rightHandPos.y >= headPos.y){
				return body;
			}
		}
		return null;
	}

	public bool HaveAquiredTrackingTarget{
		get{
			return trackedBody != null;
		}
	}
}
