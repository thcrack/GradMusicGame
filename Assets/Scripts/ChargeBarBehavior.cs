using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeBarBehavior : MonoBehaviour {

	bool isReleasingCharge = false;
	public TrackTransport chargeReleaseDuration;
	TrackTransport chargeReleaseStartTime;
	[Range(1, 999)]
	public int chargedNote = 0;
	[Range(1, 999)]
	public int maximumNotes = 50;
	public Image bar;
	Color barOriginalColor;
	public RectTransform barTopRef;
	public RectTransform barBotRef;
	public Text chargedText;
	GameManager gameManager;
	public PlayerIndicatorManager playerIndicatorManager;
	public Image chargedStandPic;
	public Image chargedSquatPic;
	public float chargedIndicatorInterval = 0.5f;
	AudioSource fullyChargedSFX;
	bool hasPlayedChargedSFX;

	void Start(){
		gameManager = Global.Instance._gameManager;
		fullyChargedSFX = GetComponent<AudioSource>();
		chargedText.gameObject.SetActive(false);
		chargedStandPic.gameObject.SetActive(false);
		chargedSquatPic.gameObject.SetActive(false);
		barOriginalColor = bar.color;
		UpdateUI();
	}

	void Update(){

		if(CrossSceneInfo.Instance.loggedID == "debug"){
			if(Input.GetKey("k")) AddValue(1);
			if(Input.GetKey("l")) ReleaseCharge();
		}
		
		if(isReleasingCharge){

			float pastInterval = gameManager.GetTrackTransport().totalPos - chargeReleaseStartTime.totalPos;
			if(pastInterval < chargeReleaseDuration.totalPos){
				UpdateUI(1f - pastInterval/chargeReleaseDuration.totalPos);
				bar.color = Color.HSVToRGB(gameManager.GetTime%1, 1f, 1f);
			}else{
				playerIndicatorManager.ToggleAttackMode(false);
				isReleasingCharge = false;
				chargedNote = 0;
				UpdateUI();
				bar.color = barOriginalColor;
			}

		}else if(FullyCharged){
			if(gameManager.gameMode == GameMode.Single){
				chargedText.gameObject.SetActive(true);
				bool chargedIndicatorState = gameManager.GetTime % chargedIndicatorInterval <= chargedIndicatorInterval/2;
				chargedStandPic.gameObject.SetActive(!chargedIndicatorState);
				chargedSquatPic.gameObject.SetActive(chargedIndicatorState);
			}
			bar.color = chargedText.color = chargedStandPic.color = chargedSquatPic.color = new Color(1.0f, 0.2f + Mathf.Sin(gameManager.GetTime * 4f) * 0.2f, 1.0f);
		}

	}

	public void UpdateValue(int value){
		chargedNote = Mathf.Clamp(value, 0, maximumNotes);
		if(FullyCharged) PlayChargedSFX();
		UpdateUI();
	}

	public void AddValue(int delta){
		chargedNote = Mathf.Clamp(chargedNote+delta, 0, maximumNotes);
		if(FullyCharged) PlayChargedSFX();
		UpdateUI();
	}

	public void UpdateUI(){
		bar.fillAmount = GetRatio;
	}

	public void UpdateUI(float ratio){
		bar.fillAmount = ratio;
	}

	public void ReleaseCharge(){
		if(!FullyCharged || isReleasingCharge) return;

		chargedText.gameObject.SetActive(false);
		chargedStandPic.gameObject.SetActive(false);
		chargedSquatPic.gameObject.SetActive(false);
		hasPlayedChargedSFX = false;
		isReleasingCharge = true;
		playerIndicatorManager.ToggleAttackMode(true);
		chargeReleaseStartTime = gameManager.GetTrackTransport();
	}

	public float GetRatio{
		get
		{
			return ((float)chargedNote)/maximumNotes;
		}
	}

	public bool FullyCharged{
		get
		{
			return !isReleasingCharge && chargedNote == maximumNotes;
		}
	}

	void PlayChargedSFX(){

		if(hasPlayedChargedSFX) return;

		fullyChargedSFX.Play();
		hasPlayedChargedSFX = true;

	}

	public Vector3 GetBarTopPosition{
		get
		{
			return Vector3.Lerp(barBotRef.position, barTopRef.position, GetRatio);
		}
	}



}
