﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrackItemBehavior : MonoBehaviour {

	public GameTrack track;
	public Text nameText;
	public Text artistText;
	public Text timeText;
	public Text difficultyText;

	[Range(0f, 1f)]
	public float previewStartPosition = 0.4f;
	public float previewDelay = 0.5f;
	public float previewFadeInDuration = 0.5f;
	public float previewFadeOutDuration = 0.25f;

	MainMenuManager mngRef;

	public void FillTextWithTrackInfo(GameTrack input){
		track = input;
		nameText.text = input.trackName;
		artistText.text = "by " + input.trackArtist;
		difficultyText.text = input.trackDifficulty.ToString();
		int m = Mathf.FloorToInt(input.trackAudioFile.length / 60);
		int s = Mathf.RoundToInt(input.trackAudioFile.length % 60);
		timeText.text = string.Format("{0:D2}:{1:D2} / {2} BPM", m, s, input.trackBPM);
		AudioSource audio = GetComponent<AudioSource>();
		audio.clip = track.trackAudioFile;
		audio.time = audio.clip.length * previewStartPosition;
	}

	public void SetManagerReference(MainMenuManager input){

		mngRef = input;

	}

	public void SelectTrack(){
		CrossSceneInfo.Instance.selectedTrack = track;
		Invoke("ChangeScene", 1f);
		mngRef.SetLayout(MenuLayout.PlaySceneTransition);
	}

	void ChangeScene(){
		SceneManager.LoadScene("Main");
	}

	public void SetAudio(){
		AudioSource audio = GetComponent<AudioSource>();
		audio.Stop();
		audio.time = audio.clip.length * previewStartPosition;
	}
}
