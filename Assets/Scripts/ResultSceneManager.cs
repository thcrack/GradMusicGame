using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ResultLayout{
	Result,
	Leaderboard,
	LayoutCount = 2
}

[System.SerializableAttribute]
public class ResultMappedGameObject{
	public GameObject gameObject;
	public ResultLayout mappedResultLayout;
	public int childrenSwitch;
	public List<IntMappedGameObject> relatedChildren;
}

public class ResultSceneManager : MonoBehaviour {

	public ResultLayout currentLayout = ResultLayout.Result;

	[Header("Layout References")]
	public List<ResultMappedGameObject> resultMappedGameObjects;
	int[] layoutSwitchStates;

	[Header("Camera Control")]
	public Transform backgroundCamera;
	public Transform resultCamAnchor;
	public Transform leaderboardCamAnchor;
	public float camPositionLerpAmount = 10f;
	public float camRotationLerpAmount = 10f;
	Dictionary<ResultLayout, Transform> camAnchors;

	[Header("Track Info")]
	public Text trackNameText;
	public Text trackArtistText;
	public Text trackTimeText;

	[Header("Sequence Entries")]
	public List<ResultSequenceEntryBehavior> sequence;
	public float startDelay = 0.5f;
	int currentSequencePosition = -1;

	void Start () {

		camAnchors = new Dictionary<ResultLayout, Transform>();
		camAnchors.Add(ResultLayout.Result, resultCamAnchor);
		camAnchors.Add(ResultLayout.Leaderboard, leaderboardCamAnchor);

		// Initialize Sequence Entries From CrossSceneInfo

		ResultScore resultScore = (CrossSceneInfo.Instance.selectedGameMode == GameMode.Single) ?
								CrossSceneInfo.Instance.singlePlayerResultScore : CrossSceneInfo.Instance.duoPlayerResultScoreA;

		sequence[0].GetComponent<Text>().text = string.Format("Hit:\n<size=50>{0}</size>\n/{1}",
																resultScore.resultHitCount,
																resultScore.resultTotalCount);
		sequence[1].targetValue = resultScore.resultHitRatio * 100;
		sequence[2].targetValue = resultScore.resultHitRatio;
		sequence[4].targetValue = resultScore.resultDefenseScore;
		sequence[6].targetValue = resultScore.resultAttackScore;
		sequence[8].targetValue = resultScore.resultTotalScore;

		// Initialize Track Info UI

		trackNameText.text = CrossSceneInfo.Instance.selectedTrack.trackName;
		trackArtistText.text = CrossSceneInfo.Instance.selectedTrack.trackArtist;
		trackTimeText.text = CrossSceneInfo.Instance.selectedTrack.GetFormattedTime();

		foreach(var e in sequence){
			e.gameObject.SetActive(false);
		}


		layoutSwitchStates = new int[(int)ResultLayout.LayoutCount];
		for(int i = 0; i < layoutSwitchStates.Length; i++){
			layoutSwitchStates[i] = 0;
		}

		SetLayout();
		SetAllLayoutChildren();

		Invoke("ToNextSequenceEntry", startDelay);
		InsertResultToDatabase();
		
	}

	void Update(){
		switch(currentLayout){
			case ResultLayout.Result:
			break;

			case ResultLayout.Leaderboard:
			break;
		}

		Transform camTarget = camAnchors[currentLayout];
		backgroundCamera.position = Vector3.Lerp(backgroundCamera.position, camTarget.position, Time.deltaTime * camPositionLerpAmount);
		backgroundCamera.rotation = Quaternion.Slerp(backgroundCamera.rotation, camTarget.rotation, Time.deltaTime * camRotationLerpAmount);

	}

	public void SetLayout(ResultLayout layout){
		currentLayout = layout;
		foreach(var o in resultMappedGameObjects){
			o.gameObject.SetActive(o.mappedResultLayout == layout);
		}
	}

	public void SetLayout(int input){
		SetLayout((ResultLayout)input);
	}

	public void SetLayout(){
		SetLayout(currentLayout);
	}

	public void SetLayoutChildren(ResultLayout layout, int childrenSwitch){

		layoutSwitchStates[(int)layout] = childrenSwitch;

		foreach(var o in resultMappedGameObjects){
			if(o.mappedResultLayout == layout){
				foreach(var c in o.relatedChildren){
					c.gameObject.SetActive(c.mappedInt == childrenSwitch);
				}
			}
		}

	}

	public void SetLayoutChildren(int input){
		SetLayoutChildren(currentLayout ,input);
	}

	public void SetLayoutChildren(){
		SetLayoutChildren(currentLayout, layoutSwitchStates[(int)currentLayout]);
	}

	public void SetAllLayoutChildren(){

		for(int i = 0; i < (int)ResultLayout.LayoutCount; i++){
			SetLayoutChildren((ResultLayout)i, layoutSwitchStates[i]);
		}

	}

	public void ToNextSequenceEntry(){
		currentSequencePosition++;
		if(currentSequencePosition >= sequence.Count) return;
		sequence[currentSequencePosition].gameObject.SetActive(true);
		sequence[currentSequencePosition].TriggerEvent(this);
	}

	public void ChangeScene(string sceneName){
		CrossSceneInfo.Instance.ChangeScene(sceneName);
	}

	public void InsertResultToDatabase(){
		if(CrossSceneInfo.Instance.selectedGameMode == GameMode.Single){

				CrossSceneInfo.Instance.lastRecord = DatabaseManager.Instance.InsertNewGameRecord(CrossSceneInfo.Instance.loggedID,
																								CrossSceneInfo.Instance.singlePlayerResultScore.resultTotalScore,
																								CrossSceneInfo.Instance.selectedTrack);
		}else{
				CrossSceneInfo.Instance.lastRecordDuoA = DatabaseManager.Instance.InsertNewGameRecord(CrossSceneInfo.Instance.loggedIDDuoA,
																								CrossSceneInfo.Instance.duoPlayerResultScoreA.resultTotalScore,
																								CrossSceneInfo.Instance.selectedTrack);
				CrossSceneInfo.Instance.lastRecordDuoB = DatabaseManager.Instance.InsertNewGameRecord(CrossSceneInfo.Instance.loggedIDDuoB,
																								CrossSceneInfo.Instance.duoPlayerResultScoreB.resultTotalScore,
																								CrossSceneInfo.Instance.selectedTrack);
		}
	}
}
