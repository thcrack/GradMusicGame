using UnityEngine;
using System.Collections;

public class Global : Singleton<Global> {

	public float defaultTime = 1f;

	public GameManager _gameManager;
	public GameTrackManager _gameTrackManager;
	public LevelGeneratorManager _levelGeneratorManager;
	public BodySourceManager _kinectBodyManager;

	void Start(){
		SetTimeScale(defaultTime);
	}

	public void SetTimeScale(float scale){
		Time.timeScale = scale;
		if(_gameTrackManager != null )_gameTrackManager.audio.pitch = scale;
	}
}