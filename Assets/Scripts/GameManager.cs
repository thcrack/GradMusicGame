using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Windows.Kinect;

public enum GameState{
	Playing,
	Paused,
	AquiringTarget,
	StateCount = 3
}

public enum GameMode{
	Single,
	Duo
}

[System.SerializableAttribute]
public class PlayerScore{
	public TextMesh hitText;
	public Text scoreText;
	public float scoreHit;
	public float scoreAttack;
	public int hitCount = 0;
	public int totalCount = 0;
	public int attackCount = 0;
	public float displayScore = 0f;
	public float targetScore = 0f;

	public void InitHit(){
		hitText.text = hitCount + "\n<size=38>/" + totalCount + "\n(0%)</size>";
	}

	public void UpdateScore(float perfectScore, int trackTotalCount, float scorePerNote, float attackScoreRatio, float deltaTime){
		scoreHit = perfectScore * hitCount / trackTotalCount;
		scoreAttack = scorePerNote * attackScoreRatio * attackCount;
		targetScore = scoreAttack + scoreHit;
		displayScore = Mathf.Lerp(displayScore, targetScore, 20f * deltaTime);
		scoreText.text = string.Format("{0:n0}", Mathf.RoundToInt(displayScore));
	}

	public void UpdateHit(bool isTriggered){
		totalCount++;
		if(isTriggered){
			hitCount++;
		}
		float p = Mathf.Floor((float)hitCount/(float)totalCount * 1000)/10;
		hitText.text = hitCount + "\n<size=38>/" + totalCount + "\n(" + p + "%)</size>";
	}

	public void UpdateAttack(){
		attackCount++;
	}
}

[System.SerializableAttribute]
public class ModeMappedGameObject{
	public GameObject gameObject;
	public GameState mappedGameState;
}

public class GameManager : MonoBehaviour {

	[Header("Core Properties")]
	public GameMode gameMode = GameMode.Single;
	public GameState gameState = GameState.Playing;
	public const int stateCount = (int)GameState.StateCount;
	public List<ModeMappedGameObject> modeMappedGameObjects;
	public TrackTransport resultDelay;

	[Header("Player Indicator References")]
	public PlayerIndicatorManager singlePlayerIndicator;
	public PlayerIndicatorManager duoPlayerIndicatorA;
	public PlayerIndicatorManager duoPlayerIndicatorB;

	[Header("Player Kinect Control References")]
	public PlayerKinectControl singlePlayerKinectControl;
	public PlayerKinectControl duoPlayerKinectControlA;
	public PlayerKinectControl duoPlayerKinectControlB;

	[Header("Mode Specific Objects")]
	public GameObject spPlayer;
	public GameObject duoPlayerA;
	public GameObject duoPlayerB;

	[Header("Track Properties")]
	public bool autoStart = true;
	public int currentTrackID = 0;
	GameTrack currentTrackInfo;
	float currentTrackMeasureInterval;

	[Header("Game Pipe Properties")]
	public int preGamePipeCount = 2;
	public int oldPipeDisableThreshold = 3;
	public int newPipeEnableThreshold = 2;
	int lastUpdatePipe;
	PipeReference[] pipes;
	int farPipeIndex;
	Vector3 farPipeAnchor;

	[Header("Score Properties")]
	public float perfectScore = 1000000000f;
	public float attackScoreRatio = 0.1f;
	public PlayerScore singlePlayerScore;
	public PlayerScore duoPlayerScoreA;
	public PlayerScore duoPlayerScoreB;
	int trackTotalCount = 0;
	float scorePerNote;
	Dictionary<PlayerIndicatorManager, PlayerScore> playerScore;

	GameTrackManager gameTrackManager;
	LevelGeneratorManager levelGeneratorManager;

	float timer = 0f;
	float startTime = 0f;
	float musicStartTime = 0f;

	// Use this for initialization
	void Start () {

		gameMode = CrossSceneInfo.Instance.selectedGameMode;
		gameTrackManager = Global.Instance._gameTrackManager;
		levelGeneratorManager = Global.Instance._levelGeneratorManager;

		InitializeTrack();

		InitScore();

		playerScore = new Dictionary<PlayerIndicatorManager, PlayerScore>();
		playerScore.Add(singlePlayerIndicator, singlePlayerScore);
		playerScore.Add(duoPlayerIndicatorA, duoPlayerScoreA);
		playerScore.Add(duoPlayerIndicatorB, duoPlayerScoreB);

		if(gameMode == GameMode.Single){
			duoPlayerA.SetActive(false);
			duoPlayerB.SetActive(false);
			spPlayer.SetActive(true);
		}else{
			spPlayer.SetActive(false);
		}
		SetGameState(gameState);

	}

	void InitializeTrack(){


		currentTrackInfo = (CrossSceneInfo.Instance.selectedTrack == null) ?
					gameTrackManager.LoadGameTrack(currentTrackID) :
					gameTrackManager.LoadGameTrack(CrossSceneInfo.Instance.selectedTrack);

		currentTrackMeasureInterval = 60f / currentTrackInfo.trackBPM * 4f;

		levelGeneratorManager.GeneratePipes(currentTrackInfo.trackMeasureCount, preGamePipeCount);
		levelGeneratorManager.GenerateNotes(currentTrackInfo.trackEvents, preGamePipeCount);

		pipes = levelGeneratorManager.GetPipes();

		farPipeIndex = newPipeEnableThreshold;
		farPipeAnchor = pipes[newPipeEnableThreshold].transform.position;
		for(int i = 0; i < newPipeEnableThreshold; i++){
			pipes[i + 1].gameObject.SetActive(true);
		}

	}

	public void InitScore(){
		trackTotalCount = GameTrackManager.FilterMidiEvents(currentTrackInfo.trackEvents).Count;
		scorePerNote = perfectScore / trackTotalCount;
		if(gameMode == GameMode.Single){
			singlePlayerScore.InitHit();
		}else{
			duoPlayerScoreA.InitHit();
			duoPlayerScoreB.InitHit();
		}
		UpdateScore();
	}
	
	// Update is called once per frame
	void Update () {

		switch(gameState){
			case GameState.Playing:
			timer += Time.deltaTime;
			if(!gameTrackManager.audio.isPlaying
				&& musicStartTime == 0f
				&& GetTrackTransport().totalPos >= 0f){

				gameTrackManager.PlayTrack();
				musicStartTime = timer;
			}

			if(GetTrackTransport().totalPos >= currentTrackInfo.trackMeasureCount){
				LoadScoreToCrossSceneInfo();
				ChangeScene("Result");
			}

			pipes[farPipeIndex].transform.position = Vector3.Lerp(pipes[farPipeIndex].transform.position, farPipeAnchor, 5f * Time.deltaTime);
			UpdateScore();

			break;

			case GameState.AquiringTarget:
			if(gameMode == GameMode.Single){
				if(singlePlayerKinectControl.HaveAquiredTrackingTarget){
					SetGameState(GameState.Playing);
					if(startTime == 0f) StartGame();
				}
			}else{
				if(duoPlayerKinectControlA.HaveAquiredTrackingTarget
				&& duoPlayerKinectControlB.HaveAquiredTrackingTarget){
					SetGameState(GameState.Playing);
					if(startTime == 0f) StartGame();
				}
			}
			break;
		}

		if(Input.GetButtonDown("Jump")) ToggleGameState();
		if(Input.GetKeyDown("r")) EndImmediately();
	}

	public void StartGame(){
		startTime = timer;
	}

	public float GetTime{
		get{
			return timer;
		}
	}

	public float GetDeltaTime{
		get{
			return (gameState == GameState.Playing) ? Time.deltaTime : 0f;
		}
	}

	public void UpdatePipes(int newPipe){
		if(newPipe <= lastUpdatePipe) return;

		if(newPipe > oldPipeDisableThreshold - 1) pipes[newPipe - oldPipeDisableThreshold].gameObject.SetActive(false);
		if(newPipe < pipes.Length - newPipeEnableThreshold){
			farPipeIndex = newPipe + newPipeEnableThreshold;
			pipes[farPipeIndex - 1].transform.position = farPipeAnchor;
			farPipeAnchor = pipes[farPipeIndex].transform.position;
			pipes[farPipeIndex].transform.position += pipes[farPipeIndex].pipeStart.forward * 400f;
			pipes[farPipeIndex].gameObject.SetActive(true);
		}

		lastUpdatePipe = newPipe;
	}

	public TrackTransport GetTrackTransport(TrackTransport offset){

		return new TrackTransport(GetTime / currentTrackMeasureInterval - preGamePipeCount + offset.totalPos);

	}

	public TrackTransport GetTrackTransport(){

		return new TrackTransport(GetTime / currentTrackMeasureInterval - preGamePipeCount);

	}

	public TrackTransport GetPipeTransport(TrackTransport offset){

		return new TrackTransport(GetTime / currentTrackMeasureInterval + offset.totalPos);

	}

	public TrackTransport GetPipeTransport(){

		return new TrackTransport(GetTime / currentTrackMeasureInterval);

	}

	public bool isInProgress{
		get{
			return timer - musicStartTime < currentTrackInfo.trackAudioFile.length;
		}
	}

	public void UpdateScore(){
		if(gameMode == GameMode.Single){
			singlePlayerScore.UpdateScore(perfectScore, trackTotalCount, scorePerNote, attackScoreRatio, GetDeltaTime);
		}else{
			duoPlayerScoreA.UpdateScore(perfectScore, trackTotalCount, scorePerNote, attackScoreRatio, GetDeltaTime);
			duoPlayerScoreB.UpdateScore(perfectScore, trackTotalCount, scorePerNote, attackScoreRatio, GetDeltaTime);
		}
	}

	public void UpdateHit(NoteBehavior n, PlayerIndicatorManager player){
		playerScore[player].UpdateHit(n.isTriggered);
	}

	public void UpdateAttack(PlayerIndicatorManager player){
		playerScore[player].UpdateAttack();
	}

	public void SetGameState(GameState g){

		gameState = g;

		switch(g){
			case GameState.Playing:
			if(musicStartTime != 0f) gameTrackManager.PlayTrack();
			break;

			case GameState.Paused:
			gameTrackManager.PauseTrack();
			break;
		}

		foreach(var o in modeMappedGameObjects){
			o.gameObject.SetActive(o.mappedGameState == g);
		}

	}

	public void SetGameState(int g){
		if(g > stateCount) g %= stateCount;
		SetGameState((GameState)g);
	}

	public void ToggleGameState(){

		SetGameState((gameState == GameState.Playing) ? GameState.Paused : GameState.Playing);

	}

	public void ChangeScene(string sceneName){
		SceneManager.LoadScene(sceneName);
	}

	public void EndImmediately(){
		LoadScoreToCrossSceneInfo();
		ChangeScene("Result");
	}

	public bool CheckBodyAssignment(Body b, PlayerKinectControl k){

		bool result = false;

		if(k == duoPlayerKinectControlA){

			result = duoPlayerKinectControlB.trackedBody != null && b == duoPlayerKinectControlB.trackedBody;

		}else{

			result = duoPlayerKinectControlA.trackedBody != null && b == duoPlayerKinectControlA.trackedBody;

		}

		return result;
	}

	void LoadScoreToCrossSceneInfo(){
		if(gameMode == GameMode.Single){
			CrossSceneInfo.Instance.singlePlayerResultScore = new ResultScore(singlePlayerScore);
		}else{
			CrossSceneInfo.Instance.duoPlayerResultScoreA = new ResultScore(duoPlayerScoreA);
			CrossSceneInfo.Instance.duoPlayerResultScoreB = new ResultScore(duoPlayerScoreB);
		}
	}
}
