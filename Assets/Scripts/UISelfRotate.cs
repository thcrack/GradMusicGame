﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelfRotate : MonoBehaviour {

	RectTransform rt;
	GameManager gameManager;
	public float rotateSpeed = 360f;
	[Range(0f, 1f)]
	public float randomRange = 0.2f;

	void Start(){
		rt = GetComponent<RectTransform>();
		gameManager = Global.Instance._gameManager;
		rotateSpeed *= 1f + Random.Range(-randomRange/2,randomRange/2);
	}
	
	// Update is called once per frame
	void Update () {
		rt.localRotation = Quaternion.Euler(rt.localRotation.eulerAngles + new Vector3(0f, 0f, rotateSpeed * gameManager.GetDeltaTime));
	}
}
