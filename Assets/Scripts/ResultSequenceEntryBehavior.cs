﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ResultSequenceEntryType{
	Normal,
	Number,
	FillImage
}

public class ResultSequenceEntryBehavior : MonoBehaviour {

	public ResultSequenceEntryType entryType;
	public bool ignoreEndTrigger;

	[Header("Time Attributes")]
	public float endDelay = 0.5f;
	public bool fixedDuration = true;
	public float duration;
	public float maxSpeed;
	bool isEnded = false;
	float startTime;
	float startValue = 0;
	public float targetValue;

	[Header("Number Mode Attributes")]
	public bool formatInteger;
	public string prefix;
	public string suffix;

	Text text;
	Image img;

	ResultSceneManager rMng;

	public void TriggerEvent(ResultSceneManager input){

		startTime = Time.time;
		rMng = input;
		switch(entryType){

			case ResultSequenceEntryType.Normal:
			Invoke("EndTrigger", endDelay);
			break;

			case ResultSequenceEntryType.Number:
			text = GetComponent<Text>();
			break;

			case ResultSequenceEntryType.FillImage:
			img = GetComponent<Image>();
			break;

		}

	}

	void Update(){
		if(!isEnded){
			switch(entryType){
				case ResultSequenceEntryType.Number:
				if(fixedDuration){
					var ratio = (Time.time - startTime) / duration;
					float value = Mathf.Lerp(startValue, targetValue, ratio);
					if(ratio > 0.9999f){
						EndTrigger();
						value = targetValue;
					}
					SetText(value);
				}else{
					var currentValue = maxSpeed * (Time.time - startTime);
					if(currentValue > targetValue){
						EndTrigger();
						currentValue = targetValue;
					}
					SetText(currentValue);
				}

				break;

				case ResultSequenceEntryType.FillImage:
				if(fixedDuration){
					var ratio = (Time.time - startTime) / duration;
					float value = Mathf.Lerp(startValue, targetValue, ratio);
					if(ratio > 0.9999f){
						EndTrigger();
						value = targetValue;
					}
					SetImageFill(value);
				}else{
					var currentValue = maxSpeed * (Time.time - startTime);
					if(currentValue > targetValue){
						EndTrigger();
						currentValue = targetValue;
					}
					SetImageFill(currentValue);
				}
				break;

			}
		}
	}

	public void EndTrigger(){

		isEnded = true;

		if(!ignoreEndTrigger) rMng.Invoke("ToNextSequenceEntry", endDelay);

	}

	void SetText(float value){
		text.text = prefix 
					+ ((formatInteger) ? string.Format("{0:n0}", Mathf.CeilToInt(value))
										: Mathf.CeilToInt(value).ToString())
					+ suffix;
	}

	void SetImageFill(float value){
		img.fillAmount = value;
	}
}
