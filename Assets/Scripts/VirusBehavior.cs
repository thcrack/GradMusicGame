﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusBehavior : MonoBehaviour {

	public Transform meshTransform;
	public GameObject projectilePrefab;
	public Transform[] projectileSpawnPoints;
	public float spinSpeed = 180f;

	Dictionary<GameNote, GameObject> projectiles;
	Dictionary<GameObject, int> projectileSpawnPointIndex;
	GameManager gameManager;

	void Start(){
		gameManager = Global.Instance._gameManager;
		projectiles = new Dictionary<GameNote, GameObject>();
		projectileSpawnPointIndex = new Dictionary<GameObject, int>();
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 newRot = meshTransform.localRotation.eulerAngles + new Vector3(0f, spinSpeed * gameManager.GetDeltaTime, 0f);
		meshTransform.localRotation = Quaternion.Euler(newRot);

		float distance = gameManager.singlePlayerIndicator.noteHintStartDistance.totalPos;
		float currentPos = Global.Instance._gameManager.GetTrackTransport().totalPos;
		foreach(KeyValuePair<GameNote, GameObject> p in projectiles){
			p.Value.transform.rotation = Quaternion.LookRotation(p.Key.noteObject.transform.position - p.Value.transform.position);
			p.Value.transform.position = Vector3.LerpUnclamped(projectileSpawnPoints[projectileSpawnPointIndex[p.Value]].position,
											p.Key.noteObject.transform.position,
											1f - (p.Key.transport.totalPos - currentPos)/distance
										);
		}
		
	}

	public void CreateProjectile(GameNote note){
		int spawnIndex = Random.Range(0, projectileSpawnPoints.Length);
		GameObject newProjectile = (GameObject) Instantiate(projectilePrefab, projectileSpawnPoints[spawnIndex]);
		newProjectile.transform.rotation = Quaternion.LookRotation(note.noteObject.transform.position - newProjectile.transform.position);
		projectiles.Add(note, newProjectile);
		projectileSpawnPointIndex.Add(newProjectile, spawnIndex);
	}

	public void DeleteProjectile(GameNote note){
		if(projectiles.ContainsKey(note)){
			GameObject targetProjectile = projectiles[note];
			projectiles.Remove(note);
			projectileSpawnPointIndex.Remove(targetProjectile);
			Destroy(targetProjectile);
		}
	}

	public void OnTriggerEnter(Collider col){
		LaserBulletBehavior laser = col.GetComponentInParent<LaserBulletBehavior>();
		if(laser != null) laser.TriggerExplosion();
	}
}
