﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBulletBehavior : MonoBehaviour {

	public float travelTime = 0.8f;
	public GameObject laser;
	public ParticleSystem particle;
	float startTime;
	bool hasExploded = false;
	Transform startAnchor, endAnchor;
	GameManager gameManager;

	// Use this for initialization
	void Start () {

		gameManager = Global.Instance._gameManager;
		startTime = gameManager.GetTime;
		var s = particle.sizeOverLifetime;
		var sS = particle.sizeOverLifetime.size;
		sS.constantMax *= Random.Range(0.5f, 1.3f);
		particle.transform.localPosition += new Vector3(Random.Range(-1.5f, 1.5f), Random.Range(-1.5f, 1.5f), Random.Range(-1.5f, 1.5f));
		
	}
	
	// Update is called once per frame
	void Update () {

		if(startAnchor != null){
			transform.position = Vector3.Lerp(startAnchor.position, endAnchor.position, (gameManager.GetTime - startTime)/travelTime);
		}

		if(hasExploded && particle.isStopped){
			Destroy(this);
		}
		
	}

	public void SetAnchor(Transform start, Transform end){
		startAnchor = start;
		endAnchor = end;
	}

	public void TriggerExplosion(){
		if(hasExploded) return;
		laser.SetActive(false);
		hasExploded = true;
		particle.Play(true);
	}
}
