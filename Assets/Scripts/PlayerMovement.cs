using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	[Header("Player Properties")]
	public Transform player;
	public bool isControllable = false;

	[Header("Children Transform References")]
	public Transform playerInnerControl;

	[Header("Transport Properties")]
	public TrackTransport transportOffset;
	TrackTransport currentTransport;

	[Header("Other")]
	public float rotateSpeed = 45f;
	public float pipeWallOffset = 0.25f;
	public float xOffset = 0f;

	int currentPipe = 0;

	PipeReference[] pipes;
	GameManager gameManager;

	void Start(){

		gameManager = Global.Instance._gameManager;
		pipes = Global.Instance._levelGeneratorManager.GetPipes();
		currentTransport = new TrackTransport(transportOffset.measure, transportOffset.measurePosition);

		if(currentTransport.measure < pipes.Length){
			transform.position = pipes[0].pipeCenter.position;
			transform.rotation = pipes[0].pipeCenter.rotation;
			playerInnerControl.localPosition = new Vector3(-pipes[currentPipe].pipeInnerRadius, 0f, 0f);
			Vector3 playerOffset = new Vector3(xOffset, -(pipes[0].pipeOuterRadius * (1f - pipeWallOffset)), 0f);
			player.localPosition = playerOffset;
		}

	}

	void Update () {

		// Check if audio is still playing
		if(gameManager.isInProgress){

			// Calculate where we are in the track, measure-wise
			currentTransport = gameManager.GetPipeTransport(transportOffset);

			// Check if we should be in the next pipe now (if there's any)
			if(currentPipe != currentTransport.measure && currentPipe + 1 < pipes.Length){

				// Make transition to next pipe
				currentPipe = currentTransport.measure;

				// Disable old pipe and activate new pipes
				gameManager.UpdatePipes(currentPipe);

				/**
				To transit to next pipe we have to change the outer arm's rotation,
				and doing so will affect the inner arm's and player's rotation since they're the outer arm's children.
				To avoid sudden change of rotation value, we memorize their original rotation,
				and restore them after we're done modifying the outer arm.
				**/

				// Memorize rotations of children
				Quaternion originalInnerRotation = playerInnerControl.rotation;

				// Move outer arm (to which this script attaches) to next pipe's reference point
				transform.position = pipes[currentPipe].pipeCenter.position;
				transform.rotation = pipes[currentPipe].pipeCenter.rotation;

				// Set the length of inner arm referring to the pipe
				playerInnerControl.localPosition = new Vector3(-pipes[currentPipe].pipeInnerRadius, 0f, 0f);

				// Restore rotations of children
				playerInnerControl.rotation = originalInnerRotation;
			}

			// player input w/o kinect
			if(isControllable) RotatePlayer(Input.GetAxis("Horizontal"));
			

			//regular update

			switch(pipes[currentPipe].pipeType){
				case PipeType.RoundQuarter:
				transform.rotation = Quaternion.Slerp(pipes[currentPipe].pipeStart.rotation, pipes[currentPipe].pipeEnd.rotation, currentTransport.measurePosition);
				break;

				case PipeType.Straight:
				transform.position = Vector3.Lerp(pipes[currentPipe].pipeStart.position, pipes[currentPipe].pipeEnd.position, currentTransport.measurePosition);
				break;
			}
		}
	}

	public void RotatePlayer(float i){
		if(i != 0f){
			Vector3 newRot = playerInnerControl.rotation.eulerAngles + new Vector3(0f, 0f, i * rotateSpeed * Time.deltaTime);
			playerInnerControl.rotation = Quaternion.Euler(newRot);
		}
	}
}
