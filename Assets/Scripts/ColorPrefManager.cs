﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ColorPrefType{
	VirusWireframe,
	PlayerWireframe,
	PipeWireframe,
	PipeBody,
	NoteStart,
	NoteMid,
	NoteEnd,
	Count = 7
}

public class ColorPrefManager : MonoBehaviour {

	public Dictionary<ColorPrefType, Color> colors;

	public ColorPrefType selectedColorType = ColorPrefType.VirusWireframe;

	public ColorPicker colorPicker;

	[Header("Material/Image Reference")]
	public Material virusWireframe;
	public Material playerWireframe;
	public Material pipeWireframe;
	public Material pipeBody;
	public Image noteStart;
	public Image noteMid;
	public Image noteEnd;

	// Use this for initialization
	void Start () {

		InitColors();

		if(CrossSceneInfo.Instance.originalColorPref == null){
			CrossSceneInfo.Instance.SetColorPref(colors);
		}


		
	}

	void InitColors(){
		colors = new Dictionary<ColorPrefType, Color>();

		colors.Add(ColorPrefType.VirusWireframe, virusWireframe.color);
		colors.Add(ColorPrefType.PlayerWireframe, playerWireframe.color);
		colors.Add(ColorPrefType.PipeWireframe, pipeWireframe.color);
		colors.Add(ColorPrefType.PipeBody, pipeBody.GetColor("_EmissionColor"));
		colors.Add(ColorPrefType.NoteStart, noteStart.color);
		colors.Add(ColorPrefType.NoteMid, noteMid.color);
		colors.Add(ColorPrefType.NoteEnd, noteEnd.color);

		colorPicker.CurrentColor = colors[selectedColorType];
	}

	public void UpdateColor(Color c){
		if(colors == null || !colors.ContainsKey(selectedColorType)) return;

		colors[selectedColorType] = c;

		switch(selectedColorType){
			case ColorPrefType.VirusWireframe:
			virusWireframe.color = c;
			break;
			case ColorPrefType.PlayerWireframe:
			playerWireframe.color = c;
			break;
			case ColorPrefType.PipeWireframe:
			pipeWireframe.color = c;
			break;
			case ColorPrefType.PipeBody:
			pipeBody.SetColor("_EmissionColor", c);
			break;
			case ColorPrefType.NoteStart:
			noteStart.color = c;
			break;
			case ColorPrefType.NoteMid:
			noteMid.color = c;
			break;
			case ColorPrefType.NoteEnd:
			noteEnd.color = c;
			break;
		}
	}

	public void ChangeTarget(ColorPrefType target){
		selectedColorType = target;
		colorPicker.CurrentColor = colors[target];
	}

	public void ChangeTarget(int i){
		ChangeTarget((ColorPrefType) i);
	}

	public void ResetColors(bool initAfterReset){
		virusWireframe.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.VirusWireframe];
		playerWireframe.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.PlayerWireframe];
		pipeWireframe.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.PipeWireframe];
		pipeBody.SetColor("_EmissionColor", CrossSceneInfo.Instance.originalColorPref[ColorPrefType.PipeBody]);
		noteStart.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.NoteStart];
		noteMid.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.NoteMid];
		noteEnd.color = CrossSceneInfo.Instance.originalColorPref[ColorPrefType.NoteEnd];
		if(initAfterReset) InitColors();
	}

	void OnApplicationQuit(){
		ResetColors(false);
	}
}
