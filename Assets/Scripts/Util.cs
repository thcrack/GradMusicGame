﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using MidiSharp;

public static class Util{

	public static Vector3 KinectCamSpaceToVector3(CameraSpacePoint input){
    	return new Vector3(input.X, input.Y, input.Z);
    }

    public static Quaternion KinectVector4ToQuaternion(Windows.Kinect.Vector4 k){
        return new Quaternion(k.X, k.Y, k.Z, k.W);
    }
}

[System.SerializableAttribute]
public class TrackTransport{
	public int measure = 0;
	[Range(0f,1f)]
	public float measurePosition = 0f;

	public float totalPos{
		get{
			return measurePosition + measure;
		}
	}

	public TrackTransport(int m, float mP){
		measure = m;
		measurePosition = mP;
	}

	public TrackTransport(float total){
		measure = Mathf.FloorToInt(total);
		measurePosition = total - measure;
	}
}

[System.SerializableAttribute]
public class GameTrack {
	
	public int trackID;
	public bool debugOnly = false;
	
	public string trackName;
	public string trackArtist;
	public float trackBPM;
	public GameTrackDifficulty trackDifficulty;
	public AudioClip trackAudioFile;
	public string trackMidiFileName;

	[HideInInspector]
	public int trackMeasureCount;
	[HideInInspector]
	public MidiEventCollection trackEvents;

	public string GetFormattedTime(){
		int m = Mathf.FloorToInt(trackAudioFile.length / 60);
		int s = Mathf.RoundToInt(trackAudioFile.length % 60);
		return string.Format("{0:D2}:{1:D2} / {2} BPM", m, s, trackBPM);
	}

	//creates a dummy track only containing trackID
	public GameTrack(int trackID){
		this.trackID = trackID;
	}

}

public enum GameTrackDifficulty{
	Easy,
	Normal,
	Hard
}

public class GameNote{
	public GameObject noteObject;
	public byte note;
	public TrackTransport transport;

	public GameNote(GameObject noteObject, byte note, TrackTransport transport){
		this.noteObject = noteObject;
		this.note = note;
		this.transport = transport;
	}
}
