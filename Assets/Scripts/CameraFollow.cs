﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public Transform originalAnchor;
	public Transform TPSLeftAnchor;
	public Transform TPSRightAnchor;
	public float TPSLerpAmount = 30f;
	public bool fixedMode = true;
	public bool smoothLerp = true;
	public float smoothing = 5f;
	public bool rotateSlerp = true;
	public float rotateSlerpAmount = 2f;

	GameManager gameManager;
	PlayerIndicatorManager playerIndicatorManager;
	PlayerKinectControl playerKinectControl;

	Vector3 offset;

	void Start(){
		offset = transform.localPosition;
		originalAnchor.localPosition = transform.localPosition;
		gameManager = Global.Instance._gameManager;
		playerIndicatorManager = gameManager.singlePlayerIndicator;
		playerKinectControl = gameManager.singlePlayerKinectControl;
	}

	void Update(){
		if(!fixedMode) CameraUpdate();
	}

	void FixedUpdate(){
		if(fixedMode) CameraUpdate();
	}

	void CameraUpdate(){

		if(gameManager.gameState == GameState.Playing){

			//parent movement

			Vector3 targetCamPos = target.position;

			transform.parent.position = (smoothLerp) ? 
				Vector3.Lerp(transform.parent.position, targetCamPos, smoothing * Time.deltaTime) : target.position;

			transform.parent.rotation = (rotateSlerp) ?
				Quaternion.Slerp(transform.parent.rotation, target.rotation, rotateSlerpAmount * Time.deltaTime) : target.rotation;

			//relative movement

			Transform localTarget = originalAnchor;

			if(playerIndicatorManager.isAttackMode){
				localTarget = (playerKinectControl.GetLean < 0f) ? TPSLeftAnchor : TPSRightAnchor;
			}

			transform.localPosition = Vector3.Lerp(transform.localPosition, localTarget.localPosition, TPSLerpAmount * gameManager.GetDeltaTime);
			transform.localRotation = Quaternion.Slerp(transform.localRotation, localTarget.localRotation, TPSLerpAmount * gameManager.GetDeltaTime);

		}
	}

}
