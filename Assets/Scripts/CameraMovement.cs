﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public float mouseMovementSpeed = 0.1f;
	Vector3 lastMousePos;

	void Start(){
		lastMousePos = Input.mousePosition;
	}
	
	// Update is called once per frame
	void Update () {

		float movementDelta = (Input.mousePosition.x - lastMousePos.x) * mouseMovementSpeed;
		Vector3 eul = transform.parent.transform.rotation.eulerAngles;

		eul += new Vector3(0f, movementDelta, 0f);
		transform.parent.transform.rotation = Quaternion.Euler(eul);

		lastMousePos = Input.mousePosition;
		
	}
}
