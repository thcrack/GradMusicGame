﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using MidiSharp;
using MidiSharp.Events;
using MidiSharp.Events.Voice;
using MidiSharp.Events.Voice.Note;

public class GameTrackManager : MonoBehaviour {

	public int currentTrack = 0;

	string midiFileFolderPath = "Assets/Resources/Midi/";
	public static int midiTicksPerQuarterNote = 24;
	public GameTrack[] gameTracks;
	public AudioSource audio;

	// Use this for initialization
	void Start () {

		audio = GetComponent<AudioSource>();
		
		for(int i = 0; i < gameTracks.Length; i++){
			gameTracks[i].trackMidiFileName = midiFileFolderPath + gameTracks[i].trackMidiFileName;
			gameTracks[i].trackID = i;
		}

		
	}

	public GameTrack LoadGameTrack(int trackID){

		if(gameTracks[trackID] == null) return null;

		currentTrack = trackID;
		var fileStream = new FileStream(gameTracks[trackID].trackMidiFileName , FileMode.Open, FileAccess.Read);
		MidiSequence midiSequence = MidiSequence.Open(fileStream);
		gameTracks[trackID].trackEvents = midiSequence.Tracks[0].Events;
		gameTracks[trackID].trackMeasureCount = GetMeasureCount(GetMidiTrackTotalLength(midiSequence.Tracks[0].Events));
		fileStream.Close();
		audio.clip = gameTracks[trackID].trackAudioFile;

		return gameTracks[trackID];
	}

	public GameTrack LoadGameTrack(GameTrack track){

		return LoadGameTrack(track.trackID);
		
	}

	public static MidiEventCollection FilterMidiEvents(MidiEventCollection c){

		MidiEventCollection result = new MidiEventCollection();

		foreach(var e in c){
			if(e.GetType() == typeof(OnNoteVoiceMidiEvent))result.Add(e);
		}

		return result;

	}

	public void PlayTrack(){
		audio.Play();
	}

	public void PlayTrack(float startTime){
		audio.time = startTime;
		audio.Play();
	}

	public void PauseTrack(){
		audio.Pause();
	}

	public float GetAudioTime(){ return audio.time; }

	public float GetAudioSampleTime(){ return audio.timeSamples/(float)audio.clip.frequency; }

	public GameTrack GetTrackInfo(int index){ return gameTracks[index]; }
	public GameTrack GetCurrentTrackInfo(){ return gameTracks[currentTrack]; }

	long GetMidiTrackTotalLength(MidiSharp.MidiEventCollection midiCol){
		long result = 0;
		foreach(var e in midiCol){
			result += e.DeltaTime;
		}
		return result;
	}

	public static int GetMeasureCount(long midiTotalLength){
		return Mathf.CeilToInt((float)midiTotalLength / (midiTicksPerQuarterNote * 16)) ;
	}

	public static int GetMeasureCountFloor(long midiTotalLength){
		return Mathf.FloorToInt((float)midiTotalLength / (midiTicksPerQuarterNote * 16)) ;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
