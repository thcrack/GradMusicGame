﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TrackItemButton : Button, IPointerEnterHandler, IPointerExitHandler {

	AudioSource audio;
	TrackItemBehavior behavior;

	bool isHovered = false;
	float hoverTimer = 0f;
	float previewStartTime = 0f;
	float previewEndTime = 0f;

	void Start(){
		GetComp();
	}

	void Update(){
		if(isHovered){
			hoverTimer += Time.deltaTime;
			if(!audio.isPlaying && hoverTimer >= behavior.previewDelay){
				previewStartTime = Time.time;
				audio.Play();
			}
			audio.volume = Mathf.Min(1f, (Time.time - previewStartTime)/behavior.previewFadeInDuration);
		}else{
			if(Time.time - previewEndTime < behavior.previewFadeOutDuration){
				audio.volume = Mathf.Max(0f, 1f - (Time.time - previewEndTime)/behavior.previewFadeOutDuration);
			}else{
				behavior.SetAudio();
			}
		}
	}

	public override void OnPointerEnter(PointerEventData eventData){

		base.OnPointerEnter(eventData);
		isHovered = true;

	}

	public override void OnPointerExit(PointerEventData eventData){

		base.OnPointerExit(eventData);
		if(audio.isPlaying) previewEndTime = Time.time;
		hoverTimer = 0f;
		isHovered = false;

	}

	void GetComp(){
		audio = GetComponent<AudioSource>();
		behavior = GetComponent<TrackItemBehavior>();
	}
}
