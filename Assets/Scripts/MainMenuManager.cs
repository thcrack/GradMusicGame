﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MenuLayout{
	StartScreen,
	MainMenu,
	TrackSelection,
	PlaySceneTransition,
	Settings,
	LayoutCount = 5
}

[System.SerializableAttribute]
public class MenuMappedGameObject{
	public GameObject gameObject;
	public MenuLayout mappedMenuLayout;
	public int childrenSwitch;
	public List<IntMappedGameObject> relatedChildren;
}

[System.SerializableAttribute]
public class IntMappedGameObject{
	public GameObject gameObject;
	public int mappedInt;
}

public class MainMenuManager : MonoBehaviour {

	public MenuLayout currentLayout = MenuLayout.StartScreen;

	[Header("Track Selection References")]
	public GameTrackManager gameTrackManager;
	public RectTransform trackListContainer;
	public GameObject trackItemPrefab;
	public float trackItemSpace = 0.1f;

	[Header("Layout References")]
	public List<MenuMappedGameObject> menuMappedGameObjects;
	int[] layoutSwitchStates;
	public Text currentIDText;

	[Header("Camera Control")]
	public Transform backgroundCamera;
	public Transform startScreenCamAnchor;
	public Transform mainMenuCamAnchor;
	public Transform trackSelectionCamAnchor;
	public Transform playSceneTransitionCamAnchor;
	public float camPositionLerpAmount = 10f;
	public float camRotationLerpAmount = 10f;
	bool isEnteringPlayScene;
	Dictionary<MenuLayout, Transform> camAnchors;

	void Start () {

		camAnchors = new Dictionary<MenuLayout, Transform>();
		camAnchors.Add(MenuLayout.MainMenu, mainMenuCamAnchor);
		camAnchors.Add(MenuLayout.StartScreen, startScreenCamAnchor);
		camAnchors.Add(MenuLayout.TrackSelection, trackSelectionCamAnchor);
		camAnchors.Add(MenuLayout.PlaySceneTransition, playSceneTransitionCamAnchor);
		camAnchors.Add(MenuLayout.Settings, mainMenuCamAnchor);

		layoutSwitchStates = new int[(int)MenuLayout.LayoutCount];
		for(int i = 0; i < layoutSwitchStates.Length; i++){
			layoutSwitchStates[i] = 0;
		}

		float itemHeight = trackItemPrefab.GetComponent<RectTransform>().sizeDelta.y * (1f + trackItemSpace*2);
		trackListContainer.sizeDelta = new Vector2(trackListContainer.sizeDelta.x, itemHeight * gameTrackManager.gameTracks.Length);
		trackListContainer.anchoredPosition = new Vector2(trackListContainer.anchoredPosition.x, -itemHeight * gameTrackManager.gameTracks.Length / 2);
		for(int i = 0; i < gameTrackManager.gameTracks.Length; i++){
			if(gameTrackManager.gameTracks[i].debugOnly && CrossSceneInfo.Instance.loggedID != "debug") continue;
			var newTrackItem = Instantiate(trackItemPrefab, trackListContainer);
			var newTrackItemBehavior = newTrackItem.GetComponent<TrackItemBehavior>();
			newTrackItemBehavior.FillTextWithTrackInfo(gameTrackManager.gameTracks[i]);
			newTrackItemBehavior.SetManagerReference(this);
			newTrackItem.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, itemHeight * -(trackItemSpace + i));
		}

		SetLayout();
		SetAllLayoutChildren();
		
	}

	void Update(){
		switch(currentLayout){
			case MenuLayout.StartScreen:
			if(layoutSwitchStates[(int)MenuLayout.StartScreen] == 0 && Input.anyKeyDown){
				SetLayoutChildren(MenuLayout.StartScreen, 1);
			}
			break;

			case MenuLayout.MainMenu:
			break;

			case MenuLayout.TrackSelection:
			break;
		}

		Transform camTarget = camAnchors[currentLayout];
		backgroundCamera.position = Vector3.Lerp(backgroundCamera.position, camTarget.position, Time.deltaTime * camPositionLerpAmount);
		backgroundCamera.rotation = Quaternion.Slerp(backgroundCamera.rotation, camTarget.rotation, Time.deltaTime * camRotationLerpAmount);

	}

	public void SetLayout(MenuLayout layout){
		currentLayout = layout;
		foreach(var o in menuMappedGameObjects){
			o.gameObject.SetActive(o.mappedMenuLayout == layout);
		}
	}

	public void SetLayout(int input){
		SetLayout((MenuLayout)input);
	}

	public void SetLayout(){
		SetLayout(currentLayout);
	}

	public void SetLayoutChildren(MenuLayout layout, int childrenSwitch){

		layoutSwitchStates[(int)layout] = childrenSwitch;

		foreach(var o in menuMappedGameObjects){
			if(o.mappedMenuLayout == layout){
				foreach(var c in o.relatedChildren){
					c.gameObject.SetActive(c.mappedInt == childrenSwitch);
				}
			}
		}

	}

	public void SetLayoutChildren(int input){
		SetLayoutChildren(currentLayout ,input);
	}

	public void SetLayoutChildren(){
		SetLayoutChildren(currentLayout, layoutSwitchStates[(int)currentLayout]);
	}

	public void SetAllLayoutChildren(){

		for(int i = 0; i < (int)MenuLayout.LayoutCount; i++){
			SetLayoutChildren((MenuLayout)i, layoutSwitchStates[i]);
		}

	}

	public void SetID(string input){
		if(input.Length == 0) return;
		currentIDText.text = "Current ID:\n" + input;
		CrossSceneInfo.Instance.loggedID = input;
		CrossSceneInfo.Instance.selectedGameMode = GameMode.Single;
		SetLayout(MenuLayout.MainMenu);
	}

	public void SetID(string input, int playerType){
		if(input.Length == 0) return;
		if(playerType == 1){
			CrossSceneInfo.Instance.loggedIDDuoA = input;
			SetLayoutChildren(4);
		}else{
			CrossSceneInfo.Instance.selectedGameMode = GameMode.Duo;
			CrossSceneInfo.Instance.loggedIDDuoB = input;
			currentIDText.text = "Current ID:\n" + CrossSceneInfo.Instance.loggedIDDuoA + " / " + input;
			SetLayout(MenuLayout.MainMenu);
		}
	}
}
