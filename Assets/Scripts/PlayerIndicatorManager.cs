using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerIndicatorManager : MonoBehaviour {

	public bool alignWithCamera = true;
	public VirusBehavior virus;

	[Header("Hand Grid Indicator")]
	public RectTransform handGridParent;
	public Color gridHoverTargetColor = Color.magenta;
	public Color gridAttackTriggerColor = Color.magenta;
	public float gridAttackTriggerScale = 2f;
	RectTransform[] handGrids;

	[Header("Note Indicator")]
	public GameObject noteIndicatorPrefab;
	public Gradient noteColor;
	public Gradient noteTriggeredColor;
	public TrackTransport noteHintStartDistance;
	public TrackTransport noteHintEndDistance;
	public TrackTransport noteTriggerTolerance;
	List<GameNote> noteIndicators;
	List<GameNote> activeNoteIndicators;

	[Header("Hand Indicator")]
	public bool enableHandsSmooth = false;
	public float handsSmooth = 30f;
	public bool handsCastSphere = false;
	public float handsRadius = 0.4f;
	public RectTransform handsPosIndicator;
	public RectTransform leftHandUI;
	public RectTransform rightHandUI;

	[Header("Charge Bar Indicator")]
	public ChargeBarBehavior chargeBar;

	[Header("Attack Mode Indicator")]
	public bool isAttackMode = false;
	public GameObject protectionSphere;
	public GameObject laserBulletPrefab;
	public Sprite normalModeSprite;
	public Sprite attackModeSprite;
	

	Vector2 leftUIRect, rightUIRect;
	Vector2 leftHandTarget, rightHandTarget;
	AudioSource sfxAudio;

	int interactiveUILayerMask;
	float lastPos = 0f;

	RaycastHit[] leftHit, rightHit;
	GameObject leftHover, rightHover;
	GameManager gameManager;

	int currentNoteIndex = -1;

	void Start(){
		sfxAudio = GetComponent<AudioSource>();
		gameManager = Global.Instance._gameManager;
		leftHit = new RaycastHit[4];
		rightHit = new RaycastHit[4];
		interactiveUILayerMask = LayerMask.GetMask("InteractiveUI");
		InitializeHandGrids();
		if(noteHintStartDistance.totalPos < 0.000001f) noteHintStartDistance = new TrackTransport(0.000001f);
		if(noteHintEndDistance.totalPos < 0.000001f) noteHintEndDistance = new TrackTransport(0.000001f);
		protectionSphere.SetActive(isAttackMode);
	}

	void Update(){
		if(alignWithCamera) transform.rotation = Camera.main.transform.rotation;

		var lastFrameHoverState = new Dictionary<HandGridBehavior, bool>();

		for(int i = 0; i < handGrids.Length; i++){
			HandGridBehavior b = handGrids[i].GetComponent<HandGridBehavior>();
			lastFrameHoverState.Add(b, b.isHovered);
			b.isHovered = false;
		}

		// left hand raycast to get hovered grid

		leftHandUI.anchoredPosition = (enableHandsSmooth)?
								Vector2.Lerp(leftHandUI.anchoredPosition, leftHandTarget, handsSmooth * Time.deltaTime)
								: leftHandTarget;
		leftHit = new RaycastHit[4];
		if(handsCastSphere){
			Physics.SphereCastNonAlloc(leftHandUI.position, handsRadius, leftHandUI.forward, leftHit, 1f, interactiveUILayerMask);
		}else{
			Physics.RaycastNonAlloc(leftHandUI.position, leftHandUI.forward, leftHit, 1f, interactiveUILayerMask);
		}

		foreach(var g in leftHit){
			if(g.collider == null) continue;
			HandGridBehavior h = g.collider.gameObject.GetComponent<HandGridBehavior>();
			if(h == null) continue;
			if(isAttackMode && !lastFrameHoverState[h]) TriggerGridAttack(h);
			h.isHovered = true;
			h.img.color = (isAttackMode) ? gridAttackTriggerColor : gridHoverTargetColor;
		}

		// right hand raycast to get hovered grid

		rightHandUI.anchoredPosition = (enableHandsSmooth)?
								Vector2.Lerp(rightHandUI.anchoredPosition, rightHandTarget, handsSmooth * Time.deltaTime)
								: rightHandTarget;
		rightHit = new RaycastHit[4];
		if(handsCastSphere){
			Physics.SphereCastNonAlloc(rightHandUI.position, handsRadius, rightHandUI.forward, rightHit, 1f, interactiveUILayerMask);
		}else{
			Physics.RaycastNonAlloc(rightHandUI.position, rightHandUI.forward, rightHit, 1f, interactiveUILayerMask);
		}
		foreach(var g in rightHit){
			if(g.collider == null) continue;
			HandGridBehavior h = g.collider.gameObject.GetComponent<HandGridBehavior>();
			if(h == null) continue;
			if(isAttackMode && !lastFrameHoverState[h]) TriggerGridAttack(h);
			h.isHovered = true;
			h.img.color = (isAttackMode) ? gridAttackTriggerColor : gridHoverTargetColor;
		}

		// Note Indicators
		float currentTransportPos = Global.Instance._gameManager.GetTrackTransport().totalPos;
		if(noteIndicators != null){
			while(currentNoteIndex + 1 < noteIndicators.Count
				&& currentTransportPos >= noteIndicators[currentNoteIndex + 1].transport.totalPos - noteHintStartDistance.totalPos){

				noteIndicators[currentNoteIndex + 1].noteObject.SetActive(true);
				virus.CreateProjectile(noteIndicators[currentNoteIndex + 1]);
				activeNoteIndicators.Add(noteIndicators[currentNoteIndex + 1]);
				currentNoteIndex ++;

			}
		}

		Vector3 triggeredNoteDestination = chargeBar.GetBarTopPosition;

		foreach(var n in activeNoteIndicators){

			NoteBehavior nBehavior = n.noteObject.GetComponent<NoteBehavior>();

			if(currentTransportPos >= n.transport.totalPos + noteHintEndDistance.totalPos){
				if(!nBehavior.isTriggered) gameManager.UpdateHit(nBehavior, this);
				virus.DeleteProjectile(n);
				n.noteObject.SetActive(false);
				continue;
			}

			if(currentTransportPos <= n.transport.totalPos){
				float timeRatio = (n.transport.totalPos - currentTransportPos)/noteHintStartDistance.totalPos;

				if(!nBehavior.isTriggered
				&& n.transport.totalPos - currentTransportPos <= noteTriggerTolerance.totalPos/2
				&& (handGrids[(int)n.note - 60].GetComponent<HandGridBehavior>().isHovered || isAttackMode)){

					nBehavior.Trigger(new TrackTransport(currentTransportPos));
					chargeBar.AddValue(1);
					gameManager.UpdateHit(nBehavior, this);
					virus.DeleteProjectile(n);

				}
				n.noteObject.GetComponent<RectTransform>().localScale = new Vector3(1f - timeRatio, 1f - timeRatio, 1f);
				n.noteObject.GetComponent<Image>().color = (nBehavior.isTriggered) ?
								noteTriggeredColor.Evaluate((1f-timeRatio)/2) : noteColor.Evaluate((1f-timeRatio)/2);
			}else{
				if(lastPos <= n.transport.totalPos) n.noteObject.transform.parent.GetComponent<RectTransform>().localScale = new Vector3(1.4f, 1.4f, 1f);
				float timeRatio = (currentTransportPos - n.transport.totalPos)/noteHintEndDistance.totalPos;
				if(!nBehavior.isTriggered
				&& currentTransportPos - n.transport.totalPos <= noteTriggerTolerance.totalPos/2
				&& (handGrids[(int)n.note - 60].GetComponent<HandGridBehavior>().isHovered || isAttackMode)){

					nBehavior.Trigger(new TrackTransport(currentTransportPos));
					chargeBar.AddValue(1);
					gameManager.UpdateHit(nBehavior, this);
					virus.DeleteProjectile(n);

				}
				n.noteObject.GetComponent<RectTransform>().localScale =
								(nBehavior.isTriggered) ? new Vector3(1f - timeRatio * 0.5f, 1f - timeRatio * 0.5f, 1f) : new Vector3(1f, 1f, 1f);
				n.noteObject.GetComponent<Image>().color = (nBehavior.isTriggered) ? 
								noteTriggeredColor.Evaluate(0.5f + timeRatio/2) : noteColor.Evaluate(0.5f + timeRatio/2);
				if(nBehavior.isTriggered) n.noteObject.transform.position = Vector3.Lerp(n.noteObject.transform.position, triggeredNoteDestination, 8f * gameManager.GetDeltaTime);
			}
		}

		lastPos = currentTransportPos;

		activeNoteIndicators.RemoveAll(n => !n.noteObject.activeSelf);
	}

	public void UpdateHandsPosition(Vector2 left, Vector2 right){

		Vector2 gridSize = handsPosIndicator.sizeDelta;

		leftHandTarget = new Vector2(-gridSize.x/2 + left.x * gridSize.x, -gridSize.y/2 + left.y * gridSize.y);
		rightHandTarget = new Vector2(-gridSize.x/2 + right.x * gridSize.x, -gridSize.y/2 + right.y * gridSize.y);

	}

	void InitializeHandGrids(){
		handGrids = new RectTransform[handGridParent.childCount];
		for(int i = 0; i < handGridParent.childCount; i++){
			handGrids[i] = handGridParent.GetChild(i) as RectTransform;
		}
	}

	public void ToggleAttackMode(bool input){

		isAttackMode = input;
		protectionSphere.SetActive(input);
		foreach(var g in handGrids){
			g.GetComponent<Image>().sprite = (input) ? attackModeSprite : normalModeSprite;
		}

	}

	public void TriggerGridAttack(HandGridBehavior h){

		gameManager.UpdateAttack(this);
		var newLaser = Instantiate(laserBulletPrefab,
								h.transform.position,
								Quaternion.LookRotation(virus.transform.position - h.transform.position));
		newLaser.GetComponent<LaserBulletBehavior>().SetAnchor(h.transform, virus.transform);
		h.transform.localScale = new Vector3(gridAttackTriggerScale, gridAttackTriggerScale, 1f);
		sfxAudio.Play();
	}

	public void CreateNoteIndicator(List<GameNote> gameNotes){
		if(handGrids==null) InitializeHandGrids();
		noteIndicators = new List<GameNote>();
		activeNoteIndicators = new List<GameNote>();
		for(int i = 0; i < gameNotes.Count; i++){
			var newNoteIndicator = Instantiate(noteIndicatorPrefab, handGrids[(int)gameNotes[i].note-60]);
			noteIndicators.Add(new GameNote(newNoteIndicator, gameNotes[i].note, gameNotes[i].transport));
			newNoteIndicator.SetActive(false);
		}
	}
}
