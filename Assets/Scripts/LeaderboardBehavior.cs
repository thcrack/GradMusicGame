﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LeaderboardMode{
	Today,
	AllTime,
	Personal
}

public class LeaderboardBehavior : MonoBehaviour {

	public LeaderboardMode mode = LeaderboardMode.Today;
	public GameObject recordEntryPrefab;
	public int recordEntryCount = 11;
	public Button alltimeButton;
	public Button todayButton;
	public Button personalButton;
	public Text totalEntryText;
	public List<string> devList;
	bool showDev = true;
	LeaderboardItemBehavior[] items;
	List<GameRecordEntry> entries;
	Dictionary<LeaderboardMode, Button> modeButton;

	void Awake(){
		modeButton = new Dictionary<LeaderboardMode, Button>();
		modeButton.Add(LeaderboardMode.Today, todayButton);
		modeButton.Add(LeaderboardMode.AllTime, alltimeButton);
		modeButton.Add(LeaderboardMode.Personal, personalButton);
		items = new LeaderboardItemBehavior[recordEntryCount];
		for(int i = 0; i < recordEntryCount; i++){
			var newEntryItem = Instantiate(recordEntryPrefab, transform);
			items[i] = newEntryItem.GetComponent<LeaderboardItemBehavior>();
		}
	}

	public void ToggleShowDev(bool input){
		showDev = input;
		FetchData();
	}

	public void FetchData(){
		modeButton[mode].interactable = false;
		GameRecordEntry lastRecord = (CrossSceneInfo.Instance.selectedGameMode == GameMode.Single) ?
										CrossSceneInfo.Instance.lastRecord : CrossSceneInfo.Instance.lastRecordDuoA;
		entries = DatabaseManager.Instance.GetGameRecordsByTrack(CrossSceneInfo.Instance.selectedTrack, mode);
		if(!showDev){
			entries.RemoveAll(e => devList.Contains(e.playerid));
		}
		int selfIndex = entries.FindIndex(e => e.recordid == lastRecord.recordid);
		totalEntryText.text = "Total Entries: " + entries.Count;
		bool selfInTopRank = selfIndex < recordEntryCount;
		if(CrossSceneInfo.Instance.selectedGameMode == GameMode.Single){
			for(int i = 0; i < recordEntryCount; i++){
				if(selfInTopRank || i < recordEntryCount - 1){
					if(i < entries.Count){
						items[i].UpdateText(i+1, entries[i].playerid, entries[i].score);
						if(i == selfIndex){
							items[i].SetSelfColor();
						}else{
							items[i].SetNormalColor();
						}
					}else{
						items[i].ClearText();
					}
				}else{
					items[i].UpdateText(selfIndex+1, lastRecord.playerid, lastRecord.score);
					items[i].SetSelfColor();
				}
			}
		}else{
			
			for(int i = 0; i < recordEntryCount; i++){
				if(selfInTopRank || i < recordEntryCount - 1){
					if(i < entries.Count){
						items[i].UpdateText(i+1, entries[i].playerid, entries[i].score);
						if(i == selfIndex){
							items[i].SetSelfColor();
						}else{
							items[i].SetNormalColor();
						}
					}else{
						items[i].ClearText();
					}
				}else{
					items[i].UpdateText(selfIndex+1, lastRecord.playerid, lastRecord.score);
					items[i].SetSelfColor();
				}
			}
		}
	}

	public void FetchData(int changeModeTo){
		modeButton[mode].interactable = true;
		mode = (LeaderboardMode)changeModeTo;
		FetchData();
	}
}
