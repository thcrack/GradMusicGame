﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteBehavior : MonoBehaviour {

	public bool isTriggered = false;
	TrackTransport triggeredTime;

	public void Trigger(TrackTransport time){
		isTriggered = true;
		triggeredTime = time;
	}

}
