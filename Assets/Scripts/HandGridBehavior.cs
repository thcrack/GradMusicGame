using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandGridBehavior : MonoBehaviour {

	public bool isHovered = false;
	public float colorLerpAmount = 20f;
	public float scaleLerpAmount = 20f;
	Color originalColor;
	[HideInInspector]
	public Image img;
	RectTransform rect;

	// Use this for initialization
	void Start () {

		img = GetComponent<Image>();
		rect = GetComponent<RectTransform>();

		originalColor = img.color;
		
	}
	
	// Update is called once per frame
	void Update () {

		img.color = Color.Lerp(img.color, originalColor, colorLerpAmount * Time.deltaTime);
		rect.localScale = Vector2.Lerp(rect.localScale, Vector2.one, scaleLerpAmount * Time.deltaTime);
	
	}
}
