using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySql.Data;
using MySql.Data.MySqlClient;

public class GameRecordEntry{
	public string playerid;
	public int score;
	public int recordid;

	public GameRecordEntry(string playerid, int score, int recordid){
		this.playerid = playerid;
		this.score = score;
		this.recordid = recordid;
	}
}

public class DatabaseManager : Singleton<DatabaseManager> {

	public string connectionKey = "server=localhost;user=root;database=gradgame;port=3306;password=nccutest;";

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}
	
	public GameRecordEntry InsertNewGameRecord(string playerid, int score, GameTrack track){
		if(playerid == "debug" || playerid == "debug2" ) return new GameRecordEntry(playerid, score, 0);
		MySqlConnection conn = new MySqlConnection(connectionKey);
		conn.Open();

		string sql = string.Format("insert into game_record(playerid, date, score, trackid) values (?playerid, ?date, ?score, ?trackid);");

		MySqlCommand cmd = new MySqlCommand(sql, conn);

		cmd.Parameters.AddWithValue("?playerid", playerid);
		cmd.Parameters.AddWithValue("?date", System.DateTime.Now.ToString("yyyy-MM-dd"));
		cmd.Parameters.AddWithValue("?score", score);
		cmd.Parameters.AddWithValue("?trackid", track.trackID);

		cmd.ExecuteScalar();
		cmd = new MySqlCommand("select recordid from game_record where recordid = last_insert_id();", conn);
		MySqlDataReader data = cmd.ExecuteReader();
		data.Read();
		int result = System.Convert.ToInt32(data["recordid"]);
		data.Close();
		conn.Close();
		return new GameRecordEntry(playerid, score, result);
	}

	public List<GameRecordEntry> GetGameRecordsByTrack(GameTrack track, LeaderboardMode mode){
		var result = new List<GameRecordEntry>();
		MySqlConnection conn = new MySqlConnection(connectionKey);
		conn.Open();
		string sql = "";
		switch(mode){
			case LeaderboardMode.Today:
			sql = "select * from game_record where trackid = ?trackid and date = ?date order by score desc";

			break;
			case LeaderboardMode.AllTime:
			sql = "select * from game_record where trackid = ?trackid order by score desc";
			break;
			case LeaderboardMode.Personal:
			sql = "select * from game_record where trackid = ?trackid and playerid = ?playerid order by score desc";
			break;
		}
					
		MySqlCommand cmd = new MySqlCommand(sql, conn);
		cmd.Parameters.AddWithValue("?trackid", track.trackID);
		cmd.Parameters.AddWithValue("?date", System.DateTime.Now.ToString("yyyy-MM-dd"));
		if(CrossSceneInfo.Instance.selectedGameMode == GameMode.Single){
			cmd.Parameters.AddWithValue("?playerid", CrossSceneInfo.Instance.loggedID);
		}else{
			cmd.Parameters.AddWithValue("?playerid", CrossSceneInfo.Instance.loggedIDDuoA);
		}
		MySqlDataReader data = cmd.ExecuteReader();
		while(data.Read()){
			GameRecordEntry newEntry = new GameRecordEntry(data["playerid"].ToString(), System.Convert.ToInt32(data["score"]), System.Convert.ToInt32(data["recordid"]));
			result.Add(newEntry);
		}
		data.Close();
		conn.Close();
		return result;
	}

	public bool CheckIDExistence(string inputID){
		MySqlConnection conn = new MySqlConnection(connectionKey);
		conn.Open();

		string sql = "select Count(*) as count from game_record where playerid = ?playerid";
		MySqlCommand cmd = new MySqlCommand(sql, conn);
		cmd.Parameters.AddWithValue("?playerid", inputID);
		MySqlDataReader data = cmd.ExecuteReader();
		data.Read();
		bool result = System.Convert.ToInt32(data["count"]) > 0;
		conn.Close();
		return result;
	}
}
