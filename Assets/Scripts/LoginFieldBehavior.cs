﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginFieldBehavior : MonoBehaviour {

	public GameObject idExistedWarning;
	public MainMenuManager menuMng;
	public int playerType = 0;
	string checkedID;

	public void CheckID(string id){
		checkedID = id;
		if(DatabaseManager.Instance.CheckIDExistence(id)){
			idExistedWarning.SetActive(true);
		}else{
			idExistedWarning.SetActive(false);
			if(playerType == 0){
				menuMng.SetID(id);
			}else{
				menuMng.SetID(id, playerType);
			}
		}
	}

	public void ForceLogin(){
		if(checkedID == null) return;
		if(playerType == 0){
			menuMng.SetID(checkedID);
		}else{
			menuMng.SetID(checkedID, playerType);
		}
		idExistedWarning.SetActive(false);
	}
}
