﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PipeType {
	Straight,
	RoundQuarter,
	PipeTypeCount
}

public class PipeReference : MonoBehaviour {

	public PipeType pipeType;
	public int pipeIndex = 0;
	public int stepDifferenceToLastPipe = 0;
	public Renderer pipeModel;
	public Transform pipeStart;
	public Transform pipeEnd;
	public Transform pipeCenter;

	public float pipeInnerRadius = 100f;
	public float pipeOuterRadius = 10f;
}
