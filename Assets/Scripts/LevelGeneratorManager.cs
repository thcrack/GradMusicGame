using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidiSharp;
using MidiSharp.Events;
using MidiSharp.Events.Voice;
using MidiSharp.Events.Voice.Note;

public class LevelGeneratorManager : MonoBehaviour {

	public GameObject firstPipe;
	public GameObject pipePrefabRoundQuarter;
	public GameObject pipePrefabStraight;
	public GameObject notePrefab;
	public GameObject trailPrefab;

	public Transform noteControlArm, noteControlInnerArm, noteReference;
	public float noteCenterDistanceRatio = 0.75f;

	public int trailSegments = 32;
	public float trailToleranceAngle = 30f;
	public int zOffsetSteps = 16;

	PipeReference[] pipes;
	List<GameNote> gameNotes;

	public void GeneratePipes(int segmentCount, int preGamePipeCount){

		pipes = new PipeReference[segmentCount + preGamePipeCount];
		pipes[0] = firstPipe.GetComponent<PipeReference>();

		Transform pipeEnd = pipes[0].pipeEnd;

		for(int i = 1; i < pipes.Length; i++){
			bool isPreGame = i < preGamePipeCount;
			var newPipe = (isPreGame) ? Instantiate(pipePrefabStraight, transform) : Instantiate(GetRandomPipeType(), transform);
			PipeReference newPipeRef = newPipe.GetComponent<PipeReference>();
			pipes[i] = newPipeRef;
			newPipeRef.pipeIndex = i;
			Transform newPipeStart = newPipeRef.pipeStart;
			Transform newPipeEnd = newPipeRef.pipeEnd;
			Vector3 headTailDiff = pipeEnd.position - newPipeStart.position;
			newPipe.transform.rotation = pipeEnd.transform.rotation;
			int randStep = (isPreGame) ? 0 : Random.Range(0, zOffsetSteps);
			newPipeRef.stepDifferenceToLastPipe = randStep;

			newPipe.transform.localRotation = Quaternion.Euler(newPipe.transform.localRotation.eulerAngles + new Vector3(0f, 0f, 360f * randStep / zOffsetSteps));
			newPipe.transform.position += headTailDiff;
			pipeEnd = newPipeEnd;
			newPipe.SetActive(false);
		}
		
	}

	public void GenerateNotes(MidiEventCollection events, int startPipe){
		long currentTime = 0;
		gameNotes = new List<GameNote>();

		// Scan through event collection and pick out OnNote events,
		// parsing them into GameNote class
		foreach(var e in events){
			currentTime += e.DeltaTime;
			int currentMeasure = GameTrackManager.GetMeasureCountFloor(currentTime);
			// When the event is on-note event
			if(e.GetType() == typeof(OnNoteVoiceMidiEvent)){
				var n = (OnNoteVoiceMidiEvent)e;
				if(n.Note > 100) continue;
				var newNote = (GameObject) Instantiate(notePrefab, pipes[currentMeasure].transform);
				gameNotes.Add(new GameNote(newNote, n.Note, new TrackTransport(currentMeasure, (float)currentTime/(GameTrackManager.midiTicksPerQuarterNote*16)%1)));
			}
		}

		// Send PlayerIndicatorManager gamenotes info
		GameManager gameManager = Global.Instance._gameManager;
		if(gameManager.gameMode == GameMode.Single){
			gameManager.singlePlayerIndicator.CreateNoteIndicator(gameNotes);
		}else{
			gameManager.duoPlayerIndicatorA.CreateNoteIndicator(gameNotes);
			gameManager.duoPlayerIndicatorB.CreateNoteIndicator(gameNotes);
		}

		// A control arm with the same movement method as the player
		// running throughout the pipes and put notes in place

		noteReference.localPosition = new Vector3(0f, -(pipes[0].pipeOuterRadius * noteCenterDistanceRatio), 0f);

		Quaternion originalInnerRotation = noteControlInnerArm.rotation;

		for(int i = 0; i < pipes.Length; i++){

			noteControlArm.position = pipes[i].pipeCenter.position;
			noteControlArm.rotation = pipes[i].pipeCenter.rotation;

			noteControlInnerArm.rotation = originalInnerRotation;
			Quaternion originalInnerLocalRotation = noteControlInnerArm.localRotation;

			int noteIndex = i - startPipe;
			List<GameNote> filteredList = gameNotes.FindAll(n => n.transport.measure == noteIndex);
			noteControlInnerArm.localPosition = new Vector3(-pipes[i].pipeInnerRadius, 0f, 0f);
			foreach(var n in filteredList){
				float pipePos = n.transport.measurePosition;
				switch(pipes[i].pipeType){
					case PipeType.RoundQuarter:
					noteControlArm.rotation = Quaternion.Slerp(pipes[i].pipeStart.rotation, pipes[i].pipeEnd.rotation, pipePos);
					break;

					case PipeType.Straight:
					noteControlArm.position = Vector3.Lerp(pipes[i].pipeStart.position, pipes[i].pipeEnd.position, pipePos);
					break;
				}

				noteControlInnerArm.localRotation = Quaternion.Euler(originalInnerLocalRotation.eulerAngles + new Vector3(0f, 0f, 15f * (n.note-62)));
				n.noteObject.transform.position = noteReference.position;
			}

			noteControlInnerArm.localRotation = originalInnerLocalRotation;

			// Get the coordinates for trail effects,
			// and move the control arm to the final position after all notes have been placed

			// Add one for the last point
			noteReference.localPosition = new Vector3(0f, -(pipes[0].pipeOuterRadius * 0.85f), 0f);
			GameObject newTrailA = (GameObject) Instantiate(trailPrefab, pipes[i].transform);
			GameObject newTrailB = (GameObject) Instantiate(trailPrefab, pipes[i].transform);
			LineRenderer newTrailRendA = newTrailA.GetComponent<LineRenderer>();
			LineRenderer newTrailRendB = newTrailB.GetComponent<LineRenderer>();
			Vector3[] resultsA = new Vector3[trailSegments + 1];
			Vector3[] resultsB = new Vector3[trailSegments + 1];
			newTrailRendA.positionCount = trailSegments + 1;
			newTrailRendB.positionCount = trailSegments + 1;

			for(int j = 0; j < trailSegments + 1; j++){
				switch(pipes[i].pipeType){
					case PipeType.RoundQuarter:
					noteControlArm.rotation = Quaternion.Slerp(pipes[i].pipeStart.rotation, pipes[i].pipeEnd.rotation, 1f * j / trailSegments);
					break;

					case PipeType.Straight:
					noteControlArm.position = Vector3.Lerp(pipes[i].pipeStart.position, pipes[i].pipeEnd.position, 1f * j / trailSegments);
					break;
				}

				noteControlInnerArm.localRotation = Quaternion.Euler(originalInnerLocalRotation.eulerAngles + new Vector3(0f, 0f, -trailToleranceAngle/2));
				resultsA[j] = noteReference.position;
				noteControlInnerArm.localRotation = Quaternion.Euler(originalInnerLocalRotation.eulerAngles + new Vector3(0f, 0f, trailToleranceAngle/2));
				resultsB[j] = noteReference.position;
			}

			newTrailRendA.SetPositions(resultsA);
			newTrailRendB.SetPositions(resultsB);

			noteControlInnerArm.localRotation = originalInnerLocalRotation;
			originalInnerRotation = noteControlInnerArm.rotation;

		}
	}

	public PipeReference[] GetPipes(){ return pipes; }

	GameObject GetRandomPipeType(){
		switch((PipeType)Random.Range(0, 2)){
			case PipeType.RoundQuarter: return pipePrefabRoundQuarter;
			case PipeType.Straight: return pipePrefabStraight;
		}
		return null;
	}
	
}
